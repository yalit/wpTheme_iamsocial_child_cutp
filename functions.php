<?php
/*
* Queuing
*/

/**
** activation theme
**/
add_action( 'wp_enqueue_scripts', 'iamsocial_child_enqueue_styles' );

function iamsocial_child_enqueue_styles() {
	$parent_style = 'iamsocial-styles';
    
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'style.css', get_template_directory_uri() . '/css/style.css' ); //rajout pour gérer spécifiquement l'enqueue spécifique de Iamsocial
    wp_enqueue_style( 'iamsocial-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

